
package View;

import javax.swing.JFrame;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
import java.io.IOException;

import OpeningMenu.OpeningMenu;
import Game.GameBoard;
import MiniGame.miniGame;
import Model.User;
import OpeningMenu.howToPlay;

public class Driver extends WindowAdapter{

    private static JFrame mainFrame;
    public static void main(String[] args) throws IOException {
       
        mainFrame=new OpeningMenu();
        mainFrame.setVisible(true);
        
        
    
        mainFrame.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e) {
                int a=JOptionPane.showConfirmDialog(null, "Are you sure ?");
                if(a==JOptionPane.YES_OPTION){
                    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
            }
        });
        
    
        
    }
    
}
