/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpeningMenu;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.*;
import java.io.IOException;

import Utilities.*;

public class BriefExplanationPanel extends JPanel{
    
     private JTextArea textArea;
     private static final Font font = new Font("Tahoma", Font.PLAIN, 16);

    
    private void initComponent() throws IOException{
        setLayout(new FlowLayout());
        
        textArea=new JTextArea(20,40);  
        textArea.setEditable(false);
        textArea.setFont(font);
        textArea.setBackground(Color.BLACK);
        textArea.setForeground(Color.WHITE);
        textArea.setLineWrap(true);
        ReadFileToTextArea.readData("dokumen\\\\Tutorial.txt",textArea);
        JScrollPane sp = new JScrollPane(textArea,JScrollPane.VERTICAL_SCROLLBAR_NEVER,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
      
        
        add(sp);
        
       
    }
    
    public BriefExplanationPanel() throws IOException{
        initComponent();
    }

   
}
