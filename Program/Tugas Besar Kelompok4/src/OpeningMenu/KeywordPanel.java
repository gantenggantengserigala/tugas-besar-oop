/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpeningMenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.*;

public class KeywordPanel extends JPanel{
    private static JLabel title;
    private static JLabel up;
    private static JLabel down;
    private static JLabel left;
    private static JLabel right;
    private static JLabel upLabel;
    private static JLabel downLabel;
    private static JLabel leftLabel;
    private static JLabel rightLabel;

    private static final Font font = new Font("Arial Black", Font.PLAIN, 16);
    private static final Font font2 = new Font("Sans Serif", Font.PLAIN, 14);

    public KeywordPanel() {
        initComponent();
        
        
    }
    
  
    
    private void initComponent(){
        title = new JLabel("CONTROL KEY :");
        title.setFont(font);
        title.setHorizontalAlignment(JLabel.HORIZONTAL);
        title.setForeground(Color.WHITE);
        
        up = new JLabel();
        up.setIcon(new ImageIcon("image\\up.png"));
        upLabel = new JLabel("Arrow Up / W - move up");
        upLabel.setFont(font2);
        upLabel.setForeground(Color.RED);
        
        down = new JLabel();
        down.setIcon(new ImageIcon("image\\down.png"));
        downLabel = new JLabel("Arrow down / S - move up");
        downLabel.setFont(font2);
        downLabel.setForeground(Color.RED);
        
        left = new JLabel();
        left.setIcon(new ImageIcon("image\\left.png"));
        leftLabel = new JLabel("Arrow left / A - move up");
        leftLabel.setFont(font2);
        leftLabel.setForeground(Color.RED);
        
        right = new JLabel();
        right.setIcon(new ImageIcon("image\\right.png"));
        rightLabel = new JLabel("Arrow right / D - move up");
        rightLabel.setFont(font2);
        rightLabel.setForeground(Color.RED);
        
        JPanel picPanel = new JPanel(new GridLayout(4, 1));
        picPanel.setBackground(Color.BLACK);
        picPanel.setForeground(Color.WHITE);
        picPanel.add(up);
        picPanel.add(upLabel);
        picPanel.add(down);
        picPanel.add(downLabel);
        picPanel.add(left);
        picPanel.add(leftLabel);
        picPanel.add(right);
        picPanel.add(rightLabel);
        
        setLayout(new BorderLayout());
        setBackground(Color.DARK_GRAY);
        setForeground(Color.WHITE);
        add(title,BorderLayout.NORTH);
        add(picPanel,BorderLayout.CENTER);
    }
    
}
