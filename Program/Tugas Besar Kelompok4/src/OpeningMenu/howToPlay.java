/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpeningMenu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import javax.swing.*;

import Utilities.PlayMusic;
import java.awt.Image;
import java.awt.Toolkit;

public class howToPlay extends JFrame{
    
    private CardLayout cl;
    private JButton briefBtn;
    private JButton scoreBtn;
    private JButton keywordBtn;
    
    public howToPlay() throws IOException{
        initComponent();
        setSize(500,500);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("How to Play");
        setResizable(false);
        setLocationRelativeTo(null);
        
        Image icon = Toolkit.getDefaultToolkit().getImage("image\\logo.png");
        setIconImage(icon);
    }
    
    
    private void initComponent() throws IOException{
        JPanel mainPanel=new JPanel();
        mainPanel.setLayout(new BorderLayout());
        JPanel cardPanel=new JPanel();
        cl=new CardLayout();
        cardPanel.setLayout(cl);
        BriefExplanationPanel p1=new BriefExplanationPanel();
        ScoringSystemPanel p2=new ScoringSystemPanel();
        KeywordPanel p3=new KeywordPanel();
        
        
        cardPanel.add(p1,"1");
        cardPanel.add(p2,"2");
        cardPanel.add(p3,"3");
        
        JPanel btnPanel=new JPanel();
        briefBtn=new JButton("Brief Explanation");
        scoreBtn=new JButton("Scoring System");
        keywordBtn=new JButton("Keyword System");
        
        btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        btnPanel.add(briefBtn);
        btnPanel.add(scoreBtn);
        btnPanel.add(keywordBtn);
        briefBtn.setBackground(Color.WHITE);
        briefBtn.setForeground(Color.BLACK);
        
        scoreBtn.setBackground(Color.WHITE);
        scoreBtn.setForeground(Color.BLACK);
        
        keywordBtn.setBackground(Color.WHITE);
        keywordBtn.setForeground(Color.BLACK);
        
        mainPanel.add(cardPanel,BorderLayout.CENTER);
        mainPanel.add(btnPanel,BorderLayout.SOUTH);
        
       add(mainPanel);
        
       class hoverAct implements MouseListener{

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if(e.getSource()==briefBtn){
                    briefBtn.setBackground(Color.BLACK);
                    briefBtn.setForeground(Color.WHITE);
                    briefBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
                }else if(e.getSource()==scoreBtn){
                    scoreBtn.setBackground(Color.BLACK);
                    scoreBtn.setForeground(Color.WHITE);
                    scoreBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
                }else if(e.getSource()==keywordBtn){
                    keywordBtn.setBackground(Color.BLACK);
                    keywordBtn.setForeground(Color.WHITE);
                    keywordBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
                }
                
                        
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if(e.getSource()==briefBtn){
                    briefBtn.setBackground(Color.WHITE);
                    briefBtn.setForeground(Color.BLACK);
                    
                }else if(e.getSource()==scoreBtn){
                    scoreBtn.setBackground(Color.WHITE);
                    scoreBtn.setForeground(Color.BLACK);
                }else if(e.getSource()==keywordBtn){
                    keywordBtn.setBackground(Color.WHITE);
                    keywordBtn.setForeground(Color.BLACK);
                }            
            }
           
        }
       
       class clickAct implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                PlayMusic.play("music\\\\FF_click.wav");
                if(e.getSource()==briefBtn){
                    cl.first(cardPanel);
                }else if(e.getSource()==scoreBtn){
                    cl.show(cardPanel,"2");
                }else if(e.getSource()==keywordBtn){
                    cl.last(cardPanel);
                }
                
            }
           
       }
       briefBtn.addMouseListener(new hoverAct());
       scoreBtn.addMouseListener(new hoverAct());
       keywordBtn.addMouseListener(new hoverAct());
       
       briefBtn.addActionListener(new clickAct());
       scoreBtn.addActionListener(new clickAct());
       keywordBtn.addActionListener(new clickAct());
       
       btnPanel.setBackground(Color.LIGHT_GRAY);
        
    }
}
