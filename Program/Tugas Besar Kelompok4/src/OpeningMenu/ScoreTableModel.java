/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpeningMenu;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

import Model.User;

public class ScoreTableModel extends AbstractTableModel{
     private ArrayList<User> list = new ArrayList<User>();
     protected String[] columnNames = new String[] {"Date", "Name","Score"};
     protected Class[] columnClasses = new Class[] {String.class, String.class,Integer.class};
     
     public void setList(ArrayList<User> list) {
          this.list = list;
          fireTableStructureChanged();
    }
     
    @Override
    public int getRowCount() {
        return list.size();
     }
    

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public Class getColumnClass(int index) {
            return columnClasses[index];
    }
    
     @Override
        public String getColumnName(int index) {
            return columnNames[index];
        }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object temp=null;
        switch(columnIndex){
                case(0): temp=((User)list.get(rowIndex)).getDate();
                break;
                case (1): temp=((User)list.get(rowIndex)).getName();
                break;
                case (2): temp=((User)list.get(rowIndex)).getScore();
                break;
        }
        return temp;
    }
    
}
