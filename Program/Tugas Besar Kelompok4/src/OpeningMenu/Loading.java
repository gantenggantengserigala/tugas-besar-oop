/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpeningMenu;

import Game.GameBoard;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

/**
 *
 * @author tershz
 */
public class Loading extends JFrame{
    
    private final int NUMBERS = 100;
    private Task task;
    private JProgressBar pb;
    private ImageIcon img;
    
    public Loading(){
        setSize(1500,900);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        
        task = new Task();
        task.addPropertyChangeListener(new MyPropertyChangeListener());
        task.execute();
        createComponents();
    }
    
    public void createComponents(){
        
        JPanel mainPanel =  new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setBackground(Color.BLACK);
        JPanel downPanel =  new JPanel();
        downPanel.setLayout(new BorderLayout());
        downPanel.setBackground(Color.BLACK);
        JPanel textPanel =  new JPanel();
        textPanel.setLayout(new BorderLayout());
        textPanel.setBackground(Color.BLACK);
        JPanel progressPanel =  new JPanel();
        progressPanel.setLayout(new BorderLayout());
        progressPanel.setBackground(Color.BLACK);
        JPanel imgPanel =  new JPanel();
        imgPanel.setLayout(new BorderLayout());
        imgPanel.setBackground(Color.BLACK);
        JPanel midPanel =  new JPanel();
        midPanel.setLayout(new BorderLayout());
        midPanel.setBackground(Color.BLACK);
        
        pb = new JProgressBar();
        pb.setValue(0);
        pb.setStringPainted(true);
        
        JLabel textLabel1 = new JLabel("Loading");
        textLabel1.setFont(new Font("Courier", Font.BOLD,42));
        textLabel1.setForeground(Color.WHITE);
        
        JLabel imgLabel = new JLabel();
        img = new ImageIcon("image\\ch.gif");
        imgLabel.setIcon(img);
        
        midPanel.add(imgLabel,BorderLayout.EAST);
        imgPanel.add(midPanel,BorderLayout.WEST);
        downPanel.add(textLabel1,BorderLayout.NORTH);
        downPanel.add(pb,BorderLayout.SOUTH);
        textPanel.add(downPanel,BorderLayout.EAST);
        mainPanel.add(textPanel,BorderLayout.SOUTH);
        mainPanel.add(imgPanel,BorderLayout.CENTER);
        add(mainPanel);
    }
    
    class Task extends SwingWorker<Void, Void> {
    
        @Override
        public Void doInBackground() {
            int progress = 0;
            //Initialize progress property.
            setProgress(0);
            while(progress <= NUMBERS) {
            setProgress(Math.min(progress, 100));
            try {
                Thread.sleep(1);
            } catch (InterruptedException ignore) {}
                progress++;
            }
            return null;
        }

        @Override
        public void done() {
            
            JFrame newFrame = new GameBoard();
            dispose();
        }
    }
    class MyPropertyChangeListener implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("progress" == evt.getPropertyName()) {
            int progress = (Integer) evt.getNewValue();
            pb.setValue(progress);
            }
        }
    }
    
//    public static void main(String[] args) {
//        JFrame frame = new Loading();
//        
//    }
}
