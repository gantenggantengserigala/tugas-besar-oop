/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpeningMenu;

import Model.User;
import Utilities.DataAkses;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ScorePanel extends JFrame{
     private Font fontTitle = new Font("Tahoma", Font.PLAIN, 20);
     private ScoreTableModel stm;
     private JProgressBar pb;
     private JTable tabel;
     private JScrollPane sp;
     private Task task;
     
     private void initComponent() throws SQLException{
        JPanel mainPanel = new JPanel();
        mainPanel.setBackground(Color.BLACK);
        mainPanel.setLayout(new BorderLayout());

        JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        labelPanel.setBackground(Color.BLACK);
        
        JPanel bPanel = new JPanel();
        bPanel.setBackground(Color.BLACK);

        JLabel label = new JLabel("Score List : ");
        label.setFont(fontTitle);
        label.setForeground(Color.LIGHT_GRAY);

        labelPanel.add(label);

        mainPanel.add(labelPanel, BorderLayout.NORTH);
        
        JLabel labelNama = new JLabel("Username : ");
        labelNama.setForeground(Color.WHITE);
        JTextField fieldNama = new JTextField(15);
        fieldNama.setFocusable(false);
        JButton searchButton = new JButton("Search");
        searchButton.setBackground(Color.GRAY);
        searchButton.setForeground(Color.LIGHT_GRAY);
        
        JPanel progressPanel=new JPanel(new BorderLayout());
        JPanel searchPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        pb=new JProgressBar(0,100);
        pb.setValue(0);
        pb.setStringPainted(true);
        searchPanel.setBackground(Color.BLACK);
        searchPanel.add(labelNama);
        searchPanel.add(fieldNama);
        searchPanel.add(searchButton);
        progressPanel.add(pb,BorderLayout.SOUTH);
        progressPanel.add(searchPanel,BorderLayout.NORTH);
        
        mainPanel.add(progressPanel, BorderLayout.SOUTH);
        
        stm=new ScoreTableModel();
        tabel = new JTable(stm);
        sp = new JScrollPane(tabel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        bPanel.add(sp);
        mainPanel.add(bPanel, BorderLayout.CENTER);

        add(mainPanel);
        showToTable(DataAkses.showUser());
       
        fieldNama.addFocusListener(new FocusAdapter(){
            @Override 
            public void focusGained(FocusEvent e) {
             fieldNama.setBackground(Color.yellow);
             }
            
            @Override
            public void focusLost(FocusEvent e) {
                fieldNama.setBackground(Color.WHITE);
            }
        });
        
          fieldNama.addMouseListener(new MouseAdapter(){
             @Override
             public void mouseClicked(MouseEvent e){
                 fieldNama.setFocusable(true);
                 fieldNama.requestFocusInWindow();
             }
         });
       
          searchButton.addActionListener(new ActionListener(){
              @Override
              public void actionPerformed(ActionEvent e){
                  String nama=fieldNama.getText();
                  User target=DataAkses.searchUser(nama);
                  if(target==null){
                      JOptionPane.showMessageDialog(null,"user not found");
                      
                  }else{
                      JOptionPane.showMessageDialog(null, target.getName()+"\n"+target.getScore()+"\n"+target.getDate());
                  }
              }
          });
     }
     
     public ScorePanel() throws SQLException {
        initComponent();
        
        setSize(485, 600);
        setLocationRelativeTo(null);
        setTitle("Score");
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        Image icon = Toolkit.getDefaultToolkit().getImage("image\\logo.png");
        setIconImage(icon);
        
        task=new Task();
        task.execute();

    }
     
     
     class Task extends SwingWorker<Void,Integer>{

        @Override
        protected Void doInBackground() throws Exception {
            int progress=1;
            while(progress<100){
            try{
                publish((int)progress);
                progress++;
                try{
                    Thread.sleep(100);
                }catch(Exception e){
                    
                }
            }catch(Exception e){
                
            }
            }
            return null;
        }
        
        @Override
        protected void process(List<Integer> chunks){
            int value=chunks.get(chunks.size()-1);
            pb.setValue(value);
        }
        
        @Override
        public void done(){
            try {
                showToTable(DataAkses.showUser());
            } catch (SQLException ex) {
                Logger.getLogger(ScorePanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         
        
     }
     private void showToTable(ArrayList<User>listUser){
         Object[][] arrObj = new Object[listUser.size()][3];
         
         int i=0;
         for (User user : listUser) {
             arrObj[i][0]=user.getName();
             arrObj[i][1]=user.getDate();
             arrObj[i][2]=user.getScore();
             i++;
             
         }
         
         String[]title={"Nama","Date","Score"};
         DefaultTableModel dtm=new DefaultTableModel(arrObj,title);
        tabel.setModel(dtm);
        sp.setViewportView(tabel);
     }
    
}
