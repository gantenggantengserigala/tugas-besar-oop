/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OpeningMenu;

import java.awt.Font;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineEvent;
import java.time.LocalDateTime; 
import java.time.format.DateTimeFormatter; 
import java.awt.Dimension;
import java.awt.HeadlessException;

import Game.GameBoard;
import Utilities.PlayMusicLoop;
import Model.User;
import java.awt.Toolkit;
import java.sql.SQLException;



public class OpeningMenu extends JFrame{
    private final int FRAME_WIDTH = 800;
    private final int FRAME_HEIGHT = 600;
    private final int FRAME_SCORE_WIDTH = 485;
    private final int FRAME_SCORE_HEIGHT = 550;
    private final int NUMBERS = 100;

    private final Font titleFont = new Font("Tahoma", Font.PLAIN, 45);
    private final Font buttonFont = new Font("Times New Roman", Font.PLAIN, 25);

    private JLabel background;
    private JButton buttonStart;
    private JButton buttonScore;
    private JButton buttonHTP;
    private JButton buttonQuit;
    private JProgressBar pb;
    private JTable tabel;
    private JScrollPane sp;
    
    private static User user;
   
    
      public OpeningMenu() {

        //frame
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setLocationRelativeTo(null); //--> diletakkan ditengah windowsnya
        setResizable(false);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        createComponents();
        Image icon = Toolkit.getDefaultToolkit().getImage("image\\logo.png");
        setIconImage(icon);
        setTitle("Treasure Hunter");
        
        PlayMusicLoop.playSound("music\\titleMusic.wav");
    }
      public static User getUser(){
          return user;
      }
      private void createComponents(){
           //label
        blinkLabel label = new blinkLabel();
        label.setText("TREASURE HUNTER");
        label.setForeground(Color.LIGHT_GRAY);
        label.setFont(titleFont);
        label.setBounds(200, 25, 400, 50);

        Thread t = new Thread(label);
        t.start();

        //background image
        ImageIcon BackgroundImage = new ImageIcon("image/background_img.png");
        Image img = BackgroundImage.getImage();
        Image tempIMG = img.getScaledInstance(900, 600, Image.SCALE_SMOOTH);
        BackgroundImage = new ImageIcon(tempIMG);
        background = new JLabel("", BackgroundImage, JLabel.CENTER);
        background.setBounds(0, 0, 900, 600);

        //button1
        buttonStart = new JButton("START");
        buttonStart.setBackground(Color.BLUE);
        buttonStart.setForeground(Color.WHITE);
        buttonStart.setFont(buttonFont);
        buttonStart.setBounds(350, 350, 125, 40);

        //button2
        buttonScore = new JButton("SCORE");
        buttonScore.setBackground(Color.MAGENTA);
        buttonScore.setForeground(Color.WHITE);
        buttonScore.setFont(buttonFont);
        buttonScore.setBounds(350, 400, 125, 40);

        //button3
        buttonHTP = new JButton("HOW TO PLAY");
        buttonHTP.setBackground(Color.ORANGE);
        buttonHTP.setForeground(Color.WHITE);
        buttonHTP.setFont(buttonFont);
        buttonHTP.setBounds(310, 450, 205, 40);

        //button4
        buttonQuit = new JButton("QUIT");
        buttonQuit.setBackground(Color.RED);
        buttonQuit.setForeground(Color.WHITE);
        buttonQuit.setFont(buttonFont);
        buttonQuit.setBounds(350, 500, 125, 40);

        background.add(label);
        background.add(buttonStart);
        background.add(buttonScore);
        background.add(buttonHTP);
        background.add(buttonQuit);

        
        add(background);
        
        buttonQuit.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                int a=JOptionPane.showConfirmDialog(null,"are you sure?");
                if(a==JOptionPane.YES_OPTION){
                    System.exit(0);
                }
            }
        });
        
        buttonHTP.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    JFrame htpFrame=new howToPlay();
                  
                } catch (IOException ex) {
                    Logger.getLogger(OpeningMenu.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        buttonStart.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                LocalDateTime myDateObj = LocalDateTime.now(); 
                DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                String formattedDate = myDateObj.format(myFormatObj); 
                
                String name="";
                name=JOptionPane.showInputDialog("insert your name :");
                if(name.equals("")){
                    JOptionPane.showMessageDialog(null,"Please insert correct name!","input Error",JOptionPane.ERROR_MESSAGE);
                }else{
                    user=new User();
                    user.setName(name);
                    user.setDate(formattedDate);
                    user.setHealthBar("6");
                    JFrame frameNew=new Loading();
                    PlayMusicLoop.stopSound();
                    PlayMusicLoop.playSound("music\\Game.wav");
                    dispose();
                }
                
                
            }
        });
        
        buttonScore.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    JFrame spPanel=new ScorePanel();
                    
                    
                }catch(HeadlessException x){} catch (SQLException ex) {
                    Logger.getLogger(OpeningMenu.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
      }
      
      
    
      
      
}
