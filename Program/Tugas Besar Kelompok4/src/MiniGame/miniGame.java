/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MiniGame;

import Model.User;
import OpeningMenu.OpeningMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import sun.audio.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.List;

import Utilities.PlayMusic;

interface MiniGameScore{
    int SCORE=5000;
}

public class miniGame extends JFrame implements MiniGameScore{
    private static int counter=0;
    private static int counterClick=0;
    private static String check="";
    private static int lastScore;
    private static int countWin=0;
    private JPanel panel;
    private Task task;
     private JButton[]arrBtn=new JButton[9];
     private User obj;
    static ArrayList<Integer>out=new ArrayList<>();
    static HashMap<Integer,String>map=new HashMap<>();
    final static String[]url={"image\\\\hat.png","image\\\\bandana.png","image\\\\suits.png","image\\\\woman.png"};
    
    public static int getLastScore(){
        return lastScore;
    }
    
  
     //randomize image
    private int getRandom(ArrayList<Integer>list){
        boolean valid=false;
        int counter;
        int temp=-1;
        Random rand=new Random();
       
        while(!valid){
            temp=list.get(rand.nextInt(list.size()));
            counter=0;
            
            for (Integer integer : out) {
                if(integer==temp){
                    counter++;
                }
                
            }
            
            if(counter!=2){
                valid=true;
                out.add(temp);
            }
            
        }
        
        return temp;
    }
    
     private HashMap<Integer,String>getHashMap(){
        HashMap<Integer,String>myMap=new HashMap<>();
        ArrayList<Integer>index=new ArrayList<>();
        index.add(0);index.add(1);index.add(2);index.add(3);
        int count;
        
        for (int i = 0; i < 9; i++) {
            if(i==4){
                continue;
            }
            count=getRandom(index);
            myMap.put(i, url[count]);
        }
        return myMap;
    }
      private void initiateHashMap(){
         map=getHashMap();
     }
      
       //GUI 
    private void initComponent(){
        panel=new JPanel();
        
        initiateHashMap();
        Font font=new Font("Alex Brush",Font.BOLD,30);
        
        for (int i = 0; i < 9; i++) {
            arrBtn[i]=new JButton("button "+(i+1));
            arrBtn[i].setFont(font);
            arrBtn[i].setBackground(Color.BLACK);
            arrBtn[i].setForeground(Color.WHITE);
        }
        
        panel.setLayout(new GridLayout(3,3));
        
        for (int i = 0; i < 9; i++) {
            panel.add(arrBtn[i]);
        }
        
        arrBtn[4].setIcon(new ImageIcon("image\\crown.png"));
        arrBtn[4].setEnabled(false);
        arrBtn[4].setText("");
        add(panel);
        
        class buttonClickEvent implements ActionListener{
            
            @Override
            public void actionPerformed(ActionEvent e) {
                counter++;
                
                if(counter<=2){
                        PlayMusic.play("music\\\\choose.wav");
                       if (e.getSource()==arrBtn[0]) {
                           arrBtn[0].setIcon(new ImageIcon(map.get(0)));
                           arrBtn[0].setText("");
                           check+="0";
                       }else if(e.getSource()==arrBtn[1]){
                           arrBtn[1].setIcon(new ImageIcon(map.get(1)));
                           arrBtn[1].setText("");
                           check+="1";
                       }else if(e.getSource()==arrBtn[2]){
                           arrBtn[2].setIcon(new ImageIcon(map.get(2)));
                           arrBtn[2].setText("");
                           check+="2";
                       }else if(e.getSource()==arrBtn[3]){
                           arrBtn[3].setIcon(new ImageIcon(map.get(3)));
                           arrBtn[3].setText("");
                           check+="3";
                       }else if(e.getSource()==arrBtn[5]){
                           arrBtn[5].setIcon(new ImageIcon(map.get(5)));
                           arrBtn[5].setText("");
                           check+="5";
                       }else if(e.getSource()==arrBtn[6]){
                           arrBtn[6].setIcon(new ImageIcon(map.get(6)));
                           arrBtn[6].setText("");
                           check+="6";
                       }else if(e.getSource()==arrBtn[7]){
                           arrBtn[7].setIcon(new ImageIcon(map.get(7)));
                           arrBtn[7].setText("");
                           check+="7";
                       }else if(e.getSource()==arrBtn[8]){
                           arrBtn[8].setIcon(new ImageIcon(map.get(8)));
                           arrBtn[8].setText("");
                           check+="8";
                       }
                    
                }
             
               if(counter==2){
                   counter=0;
                   task=new Task(check);
                   task.execute();
                   check="";
               }
               
            }
            
        }
        
        
        for (int i = 0; i < 9; i++) {
            arrBtn[i].addActionListener(new buttonClickEvent());
            arrBtn[i].addMouseListener(new MouseAdapter(){
                
                @Override
                public void mouseClicked(MouseEvent e) {
                    counterClick++;
                
                }
                
          
            });
        }
        
        
    }
    
    class Task extends SwingWorker<Void,Integer>{
        String test;
        ArrayList<Integer>angka=new ArrayList<>();
        public Task(String test){
            this.test=test;
        }
        
        @Override
        protected Void doInBackground() throws Exception {
            int progress=0;
            setProgress(0);
            
            try{
                Thread.sleep(500);
            }catch(Exception e){}
            while(progress<=2){
                setProgress(Math.min(progress, 2));
          
                progress++;
            }
                
            return null;
        }
        
        
        @Override
        public void done(){
            String[]li=test.split("");
            for (String string : li) {
                angka.add(Integer.parseInt(string));
            }
            if(map.get(angka.get(0)).equals(map.get(angka.get(1)))){
                        for (int i = 0; i < 2; i++) {
                            arrBtn[angka.get(i)].setEnabled(false);
                            PlayMusic.play("music\\\\match.wav");
                        }
                        countWin+=2;
                    }else{
                        for (int i = 0; i <2; i++) {
                            arrBtn[angka.get(i)].setIcon(null);
                            arrBtn[angka.get(i)].setText("Button "+angka.get(i));
                        }
                    }
                    
                    angka.clear();
                  
                    if(countWin==8){
                       
                         PlayMusic.play("music\\\\youWin.wav");
                        JOptionPane.showMessageDialog(panel,"You Win!");
                        lastScore=SCORE-(counterClick*100);
                        JOptionPane.showMessageDialog(null,""+lastScore,"Mini Game Score",JOptionPane.INFORMATION_MESSAGE);                        
                        String val=""+lastScore;
                        OpeningMenu.getUser().setMiniScore(val);                        
                        dispose();
                    }
        }
        
    }
    
   public miniGame(){
        initComponent();
        setLocation(0,0);
        setSize(900,900);
        setTitle("Mini game");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

}
