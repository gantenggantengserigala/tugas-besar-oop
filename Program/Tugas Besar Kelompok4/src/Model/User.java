
package Model;
import MiniGame.miniGame;


interface Modal{
    int SCORE=10000;
}

public class User implements Modal{
    private String name;
    private String miniScore="0";
    private String score;
    private String healthBar;
    private String date;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
   
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
   
    public String getHealthBar() {
        return healthBar;
    }
    
    public void setHealthBar(String healthBar){
        this.healthBar=healthBar;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiniScore() {
        return miniScore;
    }

    public void setMiniScore(String miniScore) {
        this.miniScore = miniScore;
    }

    

    public User() {
    }
    
    public User(String name, String miniScore,String healthBar,String date,String score) {
        this.name = name;
        this.miniScore = miniScore;
        this.healthBar=healthBar;
        this.date=date;
        this.score=score;
    }
    
    
}
