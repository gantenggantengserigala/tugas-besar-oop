/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JTextArea;

public class ReadFileToTextArea {
    
    public static void readData(String url,JTextArea ta) throws IOException{
        String line="";
        try{
            File f=new File(url);
            FileReader fr=new FileReader(f);
            BufferedReader br=new BufferedReader(fr);
            
            line=br.readLine();
            while(line!=null){
                line+="\n";
                ta.append(line);
                line=br.readLine();
                
            }
        }catch(FileNotFoundException e){
            System.out.println("File not found!");
        }
    }
}
