/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

public class PlayMusicLoop {
    
    private static Clip clip;
     public static void playSound(String path) {
        try {
            
            AudioInputStream audioInput = AudioSystem.getAudioInputStream(new File(path).getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioInput);
            clip.start();                             
            clip.loop(Integer.MAX_VALUE);
            
           
        } catch (Exception ex) {
            System.out.println("Playing sound error" + ex.getMessage());
            ex.printStackTrace();
        }

    }
     
     public static void stopSound(){
         try{
             if(clip.isRunning()){
                 clip.stop();
             }
             if(clip.isOpen()){
                 clip.close();
             }
         }catch (Exception e){}
     }
     
    
     
     
}
