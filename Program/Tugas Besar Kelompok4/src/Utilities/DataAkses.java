/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;


import Model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Hanjaya
 */
public class DataAkses {
    public static void addUser(User user){
        
        String query="insert into score values (?,?,?)";
        try{
            PreparedStatement st=ConnectionManager.getConnection().prepareStatement(query);
            st.setString(1,user.getName());
            st.setString(2, user.getDate());
            st.setString(3, user.getScore());
            
            st.execute();
        }catch(SQLException e){e.printStackTrace();}
    }
    
    public static ArrayList<User> showUser() throws SQLException{
        ArrayList<User> listUser=new ArrayList<>();
        
        String query="select * from score;";
        try{
            PreparedStatement st=ConnectionManager.getConnection().prepareStatement(query);
            ResultSet rs=st.executeQuery();
            while(rs.next()){
                User obj=new User();
                obj.setName(rs.getString("nama"));
                obj.setDate(rs.getString("tanggal"));
                obj.setScore(rs.getString("score"));
                listUser.add(obj);
            }
        }catch(SQLException e){}
        
        return listUser;
    }
    
    public static User searchUser(String nama){
        User m=null;
        
        String query="select * from score where nama= ? ";
        try{
            PreparedStatement st=ConnectionManager.getConnection().prepareStatement(query);
            st.setString(1, nama);
            ResultSet rs=st.executeQuery();
            while(rs.next()){
                m=new User();
                m.setName(rs.getString("nama"));
                m.setDate(rs.getString("tanggal"));
                m.setScore(rs.getString("score"));
                break;
            }
        }catch(SQLException e){
            
        }
        
        return m;
    }
    
    public static void updateUser(User u){
        String query="update score set score= ? where nama= ?";
        
        try{
            PreparedStatement st=ConnectionManager.getConnection().prepareStatement(query);
            st.setString(1, u.getName());
            st.setString(2, u.getDate());
            st.setString(3, u.getScore());
            st.execute();
        }catch(SQLException e){
               e.printStackTrace();
        }
    }
}
