
package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;


public class PlayMusic {
     public static void  play(String filepath){
        InputStream music;
        try{
            music=new FileInputStream(new File(filepath));
            AudioStream audio=new AudioStream(music);
            AudioPlayer.player.start(audio);
            
        }catch(Exception e){
            
        }
    }
}
