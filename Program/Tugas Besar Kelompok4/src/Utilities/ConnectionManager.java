/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import Model.User;


public class ConnectionManager {
    private static String server = "jdbc:mysql://localhost/treasurehunter";
    private static String username = "root";
    private static String pwd = "";
    private static Connection connection;
   
    
    //get Connection
     private static Connection logOn() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(server, username, pwd);
        } catch (ClassNotFoundException ex) {
            System.out.println("JDBC not found");
        } catch (SQLException ex) {
            System.out.println("Failed to connect to database" + ex.getMessage());
        }
        return null;
    }
     
     public static Connection getConnection() {
        if (connection == null) {
            connection = logOn();
        }
        return connection; 
    }
     
   
  

     
     
}
