/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import javax.swing.JOptionPane;
import OpeningMenu.OpeningMenu;
import java.awt.Color;
import javax.swing.ImageIcon;

/**
 *
 * @author Felix
 */
public class HeroLaserRelation extends Thread {
    
    private Laser[] arrLsr;
    private Character chr;
    ImageIcon heartfull = new ImageIcon("image\\FullHeart.png");
    ImageIcon halfheart = new ImageIcon("image\\HalfHeart.png");
    ImageIcon emptyheart = new ImageIcon("image\\EmptyHeart.png");

    public HeroLaserRelation(Character chr, Laser[] arrLsr) {
        this.chr = chr;
        this.arrLsr = arrLsr;
    }

    public void isHitLaser(Character chr, Laser[] arrLsr) {
        int length = arrLsr.length;

        for (int i = 0; i < length; i++) {
            switch (i) {
                case 0:
                    if (arrLsr[0].isVisible() && (chr.getTileX() >= 1 && chr.getTileX() <= 3) && chr.getTileY() == 6) {
                        reduceHP();
                    }
                    break;
                case 1:
                    if (arrLsr[1].isVisible() && (chr.getTileX() >= 7 && chr.getTileX() <= 24) && chr.getTileY() == 4) {
                        reduceHP();
                    }
                    break;
                case 2:
                    if (arrLsr[2].isVisible() && (chr.getTileX() == 9) && (chr.getTileY() >= 7 && chr.getTileY() <= 8)) {
                        reduceHP();
                    }
                    break;
                case 3:
                    if (arrLsr[3].isVisible() && chr.getTileX() == 9 && (chr.getTileY() >= 13 && chr.getTileY() <= 14)) {
                        reduceHP();
                    }
                    break;
                case 4:
                    if (arrLsr[4].isVisible() && chr.getTileX() == 5 && (chr.getTileY() >= 4 && chr.getTileY() <= 25)) {
                        reduceHP();
                    }
                case 5:
                    if (arrLsr[5].isVisible() && chr.getTileX() >= 1 && chr.getTileX() <=3 && (chr.getTileY() == 12)) {
                        reduceHP();
                    }
                case 6:
                    if (arrLsr[6].isVisible() && chr.getTileX() >= 1 && chr.getTileX() <=3 && (chr.getTileY() == 19)) {
                        reduceHP();
                    }
            }
        }

    }

    @Override
    public void run() {
        while (true) {
            try {
                isHitLaser(chr, arrLsr);
            } catch (Exception e) {
            }
        }
    }

    public void reduceHP() {
        int value = Integer.parseInt(OpeningMenu.getUser().getHealthBar());
        value--;
        String temp = "" + value;
        OpeningMenu.getUser().setHealthBar(temp);
        JOptionPane.showMessageDialog(null, OpeningMenu.getUser().getHealthBar());

        if (temp.equals("5")) {

            ScoreTable.heartlabel3.setIcon(halfheart);
            ScoreTable.heartPanel.setBackground(Color.GRAY);

        } else if (temp.equals("4")) {
            ScoreTable.heartlabel3.setIcon(emptyheart);
            ScoreTable.heartPanel.setBackground(Color.GRAY);
        } else if (temp.equals("3")) {
            ScoreTable.heartlabel2.setIcon(halfheart);
            ScoreTable.heartPanel.setBackground(Color.GRAY);
        } else if (temp.equals("2")) {
            ScoreTable.heartlabel2.setIcon(emptyheart);
            ScoreTable.heartPanel.setBackground(Color.GRAY);
        } else if (temp.equals("1")) {
            ScoreTable.heartlabel1.setIcon(halfheart);
            ScoreTable.heartPanel.setBackground(Color.GRAY);
        } else {
            ScoreTable.heartlabel1.setIcon(emptyheart);
            ScoreTable.heartPanel.setBackground(Color.GRAY);
        }
        if (OpeningMenu.getUser().getHealthBar().equals("0")) {
            JOptionPane.showMessageDialog(null, "Game Over");
            System.exit(0);
        }
    }

}