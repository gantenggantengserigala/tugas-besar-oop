/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import java.awt.Image;
import javax.swing.ImageIcon;


public class Character {
    private int tileX, tileY;
    private Image chr;
    private Image chr2;
    private ImageIcon img;
    private ImageIcon img2;
    
    public Character() {

        img = new ImageIcon("image//FrontMage.gif");
        chr = img.getImage();

        tileX = 1;
        tileY = 1;
    }

    public void setChr(Image chr) {
        this.chr = chr;
    }
    
     public Image getChr() {
        
        return chr;
    }

    public int getTileX() {
        return tileX;
    }

    public int getTileY() {
        return tileY;
    }

    public void move(int tx, int ty) {

        tileX += tx;
        tileY += ty;
    }
}
