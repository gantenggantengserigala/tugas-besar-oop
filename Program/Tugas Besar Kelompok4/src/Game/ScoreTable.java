/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import OpeningMenu.OpeningMenu;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author tershz
 */
public class ScoreTable extends JPanel {

    private JLabel background;
    private String health;
    private int score = 10000;
    public static JLabel heartlabel1, heartlabel2, heartlabel3;
    public static JPanel heartPanel;

    public ScoreTable() {


        //background image
        JPanel mainPanel = new JPanel();
        ImageIcon BackgroundImage = new ImageIcon("image\\Scoreboard.png");
        Image img = BackgroundImage.getImage();
        BackgroundImage = new ImageIcon(img);
        background = new JLabel("", BackgroundImage, JLabel.CENTER);
        background.setBounds(0, 0, 220, 900);

        //Label Score
        JLabel textNamaLabel = new JLabel("Nama");
        textNamaLabel.setFont(new Font("Courier", Font.BOLD, 24));
        textNamaLabel.setForeground(Color.BLACK);
        JLabel namaLabel = new JLabel(OpeningMenu.getUser().getName());
        namaLabel.setFont(new Font("Courier", Font.BOLD, 24));
        namaLabel.setForeground(Color.BLACK);

        //ScorePanel
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        topPanel.setBounds(50, 50, 100, 100);
        topPanel.setBackground(new Color(0, 0, 0, 0));

        JPanel textNamaPanel = new JPanel();
        textNamaPanel.setLayout(new BorderLayout());
        textNamaPanel.add(textNamaLabel);
        textNamaPanel.setBackground(new Color(0, 0, 0, 0));

        JPanel namaPanel = new JPanel();
        namaPanel.setLayout(new BorderLayout());
        namaPanel.add(namaLabel);
        namaPanel.setBackground(new Color(0, 0, 0, 0));

        //Heart IMG
        ImageIcon heartfull = new ImageIcon("image\\FullHeart.png");
        ImageIcon halfheart = new ImageIcon("image\\HalfHeart.png");
        ImageIcon emptyheart = new ImageIcon("image\\EmptyHeart.png");

        //Heart Label
        heartlabel1 = new JLabel();
        heartlabel2 = new JLabel();
        heartlabel3 = new JLabel();

        heartlabel1.setIcon(heartfull);
        heartlabel2.setIcon(heartfull);
        heartlabel3.setIcon(heartfull);

        //Heart Panel
        heartPanel = new JPanel();
        heartPanel.setLayout(new BorderLayout());
        heartPanel.setBounds(50, 200, 100, 300);
        heartPanel.setBackground(new Color(0, 0, 0, 0));

        JPanel textScorePanel = new JPanel();
        textScorePanel.setLayout(new BorderLayout());
        textScorePanel.setBackground(new Color(0, 0, 0, 0));

        JPanel scorePanel = new JPanel();
        scorePanel.setLayout(new BorderLayout());
        scorePanel.setBackground(new Color(0, 0, 0, 0));

        //Add item
        topPanel.add(namaPanel, BorderLayout.SOUTH);
        topPanel.add(textNamaPanel, BorderLayout.NORTH);
        
        heartPanel.add(heartlabel1, BorderLayout.NORTH);
        heartPanel.add(heartlabel2, BorderLayout.CENTER);
        heartPanel.add(heartlabel3, BorderLayout.SOUTH);

        background.add(topPanel);
        background.add(heartPanel);

        mainPanel.add(background);
        add(mainPanel);
    }

}