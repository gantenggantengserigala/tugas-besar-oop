/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import MiniGame.miniGame;
import Model.User;
import OpeningMenu.OpeningMenu;
import Utilities.DataAkses;
import Utilities.changeSizeImage;

public class Board extends JPanel implements ActionListener {

    private Timer time;
private Map map;
    private Character chr;
    private Treassure chest;
    private Portal portal;
    private String message = "";
    private JPanel mapPanel;
    private JPanel healthPanel;
    private JPanel scorePanel;
    static boolean keyStatus = false;
    static int counter = 0;
    static int counter1 = 0;
    static int counter2 = 0;
    static int counter3 = 0;
    static int counter4 = 0;
    static int counterTrs = 0;
    static int counterKey = 0;
    static int counterMiniGame = 0;
    static int counterWin = 0;
    private static int heroCoordX;
    private static int heroCoordY;
    Image temp;
    ImageIcon img1;
    ImageIcon img2;
    ImageIcon img3;
    ImageIcon img4;

    public int getCoordX() {
        return chr.getTileX();
    }

    public int getCoordY() {
        return chr.getTileY();

    }

    public Character getChr() {
        return chr;
    }

    public Board() {
        //Panel
        //win.setIcon(new ImageIcon(changeSizeImage.resizeImage("C:\\Users\\Felix\\Documents\\NetBeansProjects\\TreasureHunter\\image\\crown.png", 32, 32)));
        chr = new Character();
        portal = new Portal();
        map = new Map();
        chest = new Treassure();
        time = new Timer(25, this);
        time.start();
        addKeyListener(new ActionListener());
        setFocusable(true);

    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        while (map.getMap(chr.getTileX(), chr.getTileY()).equals("t") && counter1 == 0) {
            if (map.getMap(chr.getTileX(), chr.getTileY()).equals("t") && counter1 == 0) {
                JOptionPane.showMessageDialog(null, "You got a key!");
                counter1++;
            }
        }

        while (map.getMap(chr.getTileX(), chr.getTileY()).equals("a") && counter2 == 0) {
            if (map.getMap(chr.getTileX(), chr.getTileY()).equals("a") && counter2 == 0) {
                JOptionPane.showMessageDialog(null, "You got a key!");
                counter2++;
            }
        }

        while (map.getMap(chr.getTileX(), chr.getTileY()).equals("b") && counter3 == 0) {
            if (map.getMap(chr.getTileX(), chr.getTileY()).equals("b") && counter3 == 0) {
                JOptionPane.showMessageDialog(null, "You got a key!");
                counter3++;
            }
        }

        while (map.getMap(chr.getTileX(), chr.getTileY()).equals("c") && counter4 == 0) {
            if (map.getMap(chr.getTileX(), chr.getTileY()).equals("c") && counter4 == 0) {
                JOptionPane.showMessageDialog(null, "You got a key!");
                counter4++;
            }
        }

        if (map.getMap(chr.getTileX(), chr.getTileY()).equals("g") && counter1 == 1 && counter2 == 1 && counter3 == 1 && counter4 == 1) {
            if (counterMiniGame == 0) {
                miniGame minGame = new miniGame();

                counterMiniGame++;
            }
            chr.move(0, -1);

        } else if (map.getMap(chr.getTileX(), chr.getTileY()).equals("g")) {
            JOptionPane.showMessageDialog(null, "You dont have enough key! Collect Them All!!");
            chr.move(0, 1);
            counter++;
        }

        if (map.getMap(chr.getTileX(), chr.getTileY()).equals("T") && counterWin == 0 && counterMiniGame == 1) {
            JOptionPane.showMessageDialog(null, "Yeay You Complete The Game!");
            User obj = OpeningMenu.getUser();
            int score = 0;
            score += Integer.parseInt(obj.getHealthBar()) * 1000;
            score += Integer.parseInt(obj.getMiniScore());
            String temp=""+score;
            obj.setScore(temp);
            if(DataAkses.searchUser(obj.getName())==null){
                DataAkses.addUser(obj);
            }else{
                User oldUser=DataAkses.searchUser(obj.getName());
                if(Integer.parseInt(oldUser.getScore()) < Integer.parseInt(obj.getScore())){
                    DataAkses.updateUser(obj);
                }
            }
            JOptionPane.showMessageDialog(null,obj.getDate()+"\n"+obj.getName()+"\n"+obj.getScore()+"\n\n");
            System.exit(0);

        } else if (map.getMap(chr.getTileX(), chr.getTileY()).equals("T") && counterWin == 0) {
            JOptionPane.showMessageDialog(null, "You dont finish the Mini Game!");
            chr.move(-1, 0);

        }

        /*else if (map.getMap(chr.getTileX(), chr.getTileY()).equals("g") && counter1 == 1 && counter2 == 1 && counter3 == 1 && counter4 == 1 && counter == 0) {
            JOptionPane.showMessageDialog(null, "Go to the minigame for complete the Game!");
        }*/
        //arr.x = arr.x + arr.velX;
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        for (int y = 0; y < 27; y++) {
            for (int x = 0; x < 27; x++) {
                if (map.getMap(x, y).equals("W")) {
                    g.drawImage(map.getWall(), x * 32, y * 32, null);
                }
                if (map.getMap(x, y).equals("F")) {
                    g.drawImage(map.getFloor(), x * 32, y * 32, null);
                }

                if (map.getMap(x, y).equals("T")) {
                    g.drawImage(map.getFloor(), x * 32, y * 32, this);
                    g.drawImage(chest.getTrp(), x * 32, y * 32, null);
                }

                if (map.getMap(x, y).equals("t")) {
                    g.drawImage(map.getFloor(), x * 32, y * 32, null);
                    g.drawImage(chest.getTrs(), x * 32, y * 32, null);
                }

                if (map.getMap(x, y).equals("w")) {
                    g.drawImage(map.getFloor(), x * 32, y * 32, null);
                    g.drawImage(chest.getTrs(), x * 32, y * 32, null);
                }

                if (map.getMap(x, y).equals("a")) {
                    g.drawImage(map.getFloor(), x * 32, y * 32, null);
                    g.drawImage(chest.getTrs2(), x * 32, y * 32, null);
                }
                if (map.getMap(x, y).equals("b")) {
                    g.drawImage(map.getFloor(), x * 32, y * 32, null);
                    g.drawImage(chest.getTrs3(), x * 32, y * 32, null);
                }
                if (map.getMap(x, y).equals("c")) {
                    g.drawImage(map.getFloor(), x * 32, y * 32, null);
                    g.drawImage(chest.getTrs4(), x * 32, y * 32, null);
                }
                if (map.getMap(x, y).equals("g")) {
                    g.drawImage(map.getFloor(), x * 32, y * 32, null);
                    g.drawImage(portal.getPortal(), x * 32, y * 32, null);
                }

            }
        }

        g.drawString(message, 200, 200);
        g.drawImage(chr.getChr(), chr.getTileX() * 32, chr.getTileY() * 32, null);

    }

    public class ActionListener extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();

            if (keyCode == KeyEvent.VK_W || keyCode == KeyEvent.VK_UP) {
                if (!map.getMap(chr.getTileX(), chr.getTileY() - 1).equals("W")) {
                    img1 = new ImageIcon("image//BackMage.gif");
                    temp = img1.getImage();
                    chr.setChr(temp);
                    chr.move(0, -1);

                }
            }
            if (keyCode == KeyEvent.VK_S || keyCode == KeyEvent.VK_DOWN) {
                if (!map.getMap(chr.getTileX(), chr.getTileY() + 1).equals("W")) {
                    img2 = new ImageIcon("image//FrontMage.gif");
                    temp = img2.getImage();
                    chr.setChr(temp);
                    chr.move(0, 1);
                }
            }
            if (keyCode == KeyEvent.VK_A || keyCode == KeyEvent.VK_LEFT) {
                if (!map.getMap(chr.getTileX() - 1, chr.getTileY()).equals("W")) {
                    img3 = new ImageIcon("image//LeftMage.gif");
                    temp = img3.getImage();
                    chr.setChr(temp);

                    chr.move(-1, 0);
                }
            }
            if (keyCode == KeyEvent.VK_D || keyCode == KeyEvent.VK_RIGHT) {
                if (!map.getMap(chr.getTileX() + 1, chr.getTileY()).equals("W")) {
                    img4 = new ImageIcon("image//RightMage.gif");
                    temp = img4.getImage();
                    chr.setChr(temp);
                    chr.move(1, 0);

                }
            }
        }

    }
}