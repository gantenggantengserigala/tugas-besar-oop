/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Felix
 */
public class Obstacle extends JLabel implements Runnable {
    
    private int generic=this.getX();
    private int x;
    private int y=this.getY()+40;
    
    @Override
    public void run() {
        x=generic;

        while (true) {
            if (x < 230) {
                x++;
            } else {
                x = generic;
            }
            this.setLocation(x, y);
            repaint();
           
            try {
                Thread.sleep(5);
            } catch (Exception e) {
            }
        }
    }
    
    @Override
    public int getX() {
        return x;
    }
    
    @Override
    public int getY() {
        return y;
    }
}