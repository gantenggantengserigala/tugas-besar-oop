
package Game;

import java.awt.Image;
import javax.swing.ImageIcon;

public class Portal {
    private Image portal;
    
    public Portal(){
        ImageIcon img = new ImageIcon ("image//gate.png");
        portal = img.getImage();
    }
    
     public Image getPortal() {
        return portal;
    }
}
