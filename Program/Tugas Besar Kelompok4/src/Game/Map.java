/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class Map {
    private Scanner m;
    private String [] map = new String [30];
    private Image wall , floor;
    
    public Map (){
        
        ImageIcon image = new ImageIcon("image//Wall.gif");
        wall = image.getImage();
        image = new ImageIcon ("image//Floor.gif");
        floor = image.getImage();
        
        openFile();
        readFile();
        closeFile();
        
    }
    
    public Image getWall (){
        return wall;
    }
    
    public Image getFloor (){
        return floor;
    }
    
    public String getMap (int x, int y){
        String index = map[y].substring(x, x + 1);
        
        return index;
    }
    
    public void openFile(){
        try{
            m = new Scanner (new File("image//Map.txt"));
        }catch (FileNotFoundException e){
            System.out.println("Error open file");
        }
    }
    
    public void readFile(){
        while(m.hasNext()){
            for (int i = 0; i < 30; i++) {
                map[i] = m.next();
            }
        }
    }
    
    public void closeFile(){
        m.close();
    }
}
