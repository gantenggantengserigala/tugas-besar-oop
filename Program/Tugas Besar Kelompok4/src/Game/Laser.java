/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import javax.swing.JLabel;

/**
 *
 * @author Felix
 */
public class Laser extends JLabel implements Runnable {

    @Override
    public void run() {
        int counter = 0;
        
        while(true){
            if(counter == 0){
                setVisible(true);
                counter = 1;
            } else {
                counter = 0;
                setVisible(false);
            }

            try{
                Thread.sleep(3500);
            }catch (Exception e){}
        }
    }   
}