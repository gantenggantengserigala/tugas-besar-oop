package Game;

import java.awt.BorderLayout;
import javax.swing.*;

import Utilities.PlayMusicLoop;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import Utilities.changeSizeImage;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GameBoard extends JFrame {

    private Obstacle arrow;
    private Laser[] lsr = new Laser[7];

    public GameBoard() {
        initComponent();

        Image icon = Toolkit.getDefaultToolkit().getImage("image\\logo.png");
        setIconImage(icon);


        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int a = JOptionPane.showConfirmDialog(null, "Are you sure ?");
                if (a == JOptionPane.YES_OPTION) {
                    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
            }
        });

    }

    public int ArrowgetX() {
        return arrow.getX();
    }

    public int ArrowgetY() {
        return arrow.getY();
    }

    private void initComponent() {
        setLayout(null);

        //obs Arrow
        arrow = new Obstacle();
        arrow.setIcon(new ImageIcon("image\\Arrow.gif"));
        arrow.setBounds(40, 300, 50, 50);
        arrow.setVisible(true);
        add(arrow);
        Thread s = new Thread(arrow);
        s.start();

        //obs Laser
        lsr[0] = new Laser();
        lsr[0].setIcon(new ImageIcon(changeSizeImage.resizeImage("image\\laser2.png", 100, 50)));
        lsr[0].setBounds(30, 180, 100, 50);
        add(lsr[0]);
        Thread ls1 = new Thread(lsr[0]);
        ls1.start();

        lsr[1] = new Laser();
        lsr[1].setIcon(new ImageIcon(changeSizeImage.resizeImage("image\\laser2.png", 1000, 50)));
        lsr[1].setBounds(220, 120, 580, 50);
        add(lsr[1]);
        Thread ls2 = new Thread(lsr[1]);
        ls2.start();

        lsr[2] = new Laser();
        lsr[2].setIcon(new ImageIcon(changeSizeImage.resizeImage("image\\laser2.png", 100, 50)));
        lsr[2].setBounds(290, 240, 30, 50);
        add(lsr[2]);
        Thread ls3 = new Thread(lsr[2]);
        ls3.start();

        lsr[3] = new Laser();
        lsr[3].setIcon(new ImageIcon(changeSizeImage.resizeImage("image\\laser2.png", 100, 50)));
        lsr[3].setBounds(290, 440, 30, 50);
        add(lsr[3]);
        Thread ls4 = new Thread(lsr[3]);
        ls4.start();

        lsr[4] = new Laser();
        lsr[4].setIcon(new ImageIcon(changeSizeImage.resizeImage("image\\laser2v.png", 100, 1000)));
        lsr[4].setBounds(127, 130, 100, 800);
        add(lsr[4]);
        Thread ls5 = new Thread(lsr[4]);
        ls5.start();

        lsr[5] = new Laser();
        lsr[5].setIcon(new ImageIcon(changeSizeImage.resizeImage("image\\laser2.png", 100, 50)));
        lsr[5].setBounds(30, 380, 100, 50);
        add(lsr[5]);
        Thread ls6 = new Thread(lsr[5]);
        ls6.start();

        lsr[6] = new Laser();
        lsr[6].setIcon(new ImageIcon(changeSizeImage.resizeImage("image\\laser2.png", 100, 50)));
        lsr[6].setBounds(30, 600, 100, 50);
        add(lsr[6]);
        Thread ls7 = new Thread(lsr[6]);
        ls7.start();
        
        /*Test score board*/
        
        JPanel panelScore = new ScoreTable();
        JLabel testLabel = new JLabel("Test");
        panelScore.setBounds(860, -10,220, 930);
        panelScore.add(testLabel);
        
        Board boardPanel = new Board();
        boardPanel.setBounds(0, 0, 860, 900);
        add(panelScore);
        add(boardPanel);
//        boardPanel.add(testPanel);
//        boardPanel.setVisible(false);

        setTitle("Maze Map");

        setSize(1096, 880);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        HeroLaserRelation hlr = new HeroLaserRelation(boardPanel.getChr(), lsr);
        Thread obs2 = new Thread(hlr);
        obs2.start();

        HeroArrowRelation hra = new HeroArrowRelation(arrow, boardPanel.getChr());
        Thread obs1 = new Thread(hra);
        obs1.start();
    }

}
