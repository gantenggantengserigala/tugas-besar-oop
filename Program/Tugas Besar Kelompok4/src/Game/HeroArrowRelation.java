/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import javax.swing.JOptionPane;
import OpeningMenu.OpeningMenu;
import java.awt.Color;
import javax.swing.ImageIcon;

/**
 *
 * @author Felix
 */
public class HeroArrowRelation extends Thread {

    Obstacle obj;
    Character chr;
    
    ImageIcon heartfull = new ImageIcon("image\\FullHeart.png");
    ImageIcon halfheart = new ImageIcon("image\\HalfHeart.png");
    ImageIcon emptyheart = new ImageIcon("image\\EmptyHeart.png");
    public HeroArrowRelation(Obstacle obj, Character chr) {
        this.chr = chr;
        this.obj = obj;
    }

    public void isHit(int x, int y, int x1, int y1) {
        //Arrow - char
        System.out.println((x / 26) + " " + x1);
        System.out.println((y / 20) + " " + y1);
        System.out.println("========================\n\n");
        if ((x / 26) == x1 && (y / 20) == y1) {
            int value = Integer.parseInt(OpeningMenu.getUser().getHealthBar());
            value--;
            String temp = "" + value;
            OpeningMenu.getUser().setHealthBar(temp);
            JOptionPane.showMessageDialog(null, OpeningMenu.getUser().getHealthBar());
            
            if(temp.equals("5")){
                ScoreTable.heartlabel3.setIcon(halfheart);
                ScoreTable.heartPanel.setBackground(Color.GRAY);
                 
            }else if(temp.equals("4")){
                ScoreTable.heartlabel3.setIcon(emptyheart);
                ScoreTable.heartPanel.setBackground(Color.GRAY);
            }else if(temp.equals("3")){
                ScoreTable.heartlabel2.setIcon(halfheart);
                ScoreTable.heartPanel.setBackground(Color.GRAY);
            }else if(temp.equals("2")){
                ScoreTable.heartlabel2.setIcon(emptyheart);
                ScoreTable.heartPanel.setBackground(Color.GRAY);
            }else if(temp.equals("1")){
                ScoreTable.heartlabel1.setIcon(halfheart);
                ScoreTable.heartPanel.setBackground(Color.GRAY);
            }else{
                ScoreTable.heartlabel1.setIcon(emptyheart);
                ScoreTable.heartPanel.setBackground(Color.GRAY);
            }
            
            if (OpeningMenu.getUser().getHealthBar().equals("0")) {
                JOptionPane.showMessageDialog(null, "Game Over");
                System.exit(0);
            }
        }
    }

    @Override
    public void run() {
        while (true) {
            isHit(obj.getX(), obj.getY(), chr.getTileX(), chr.getTileY());
            try {

            } catch (Exception e) {
            }
        }

    }

}
